# CircularDigitalClock


The sample app shows a digital clock using a circular progress bar animation.
By clicking on the floating action button, the time displayed on the screen will be saved in Room database.
To display the list of times from the database, just swiping left.

![CircularDigitalClock](https://bitbucket.org/SullyW/circulardigitalclock/raw/master/images/CircularDigitalClock.png)

![CircularDigitalClock](https://bitbucket.org/SullyW/circulardigitalclock/raw/master/images/CircularDigitalClock_landscape.png)

