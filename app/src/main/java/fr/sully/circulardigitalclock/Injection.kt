package fr.sully.circulardigitalclock

import android.content.Context
import fr.sully.circulardigitalclock.persistence.AppDatabase
import fr.sully.circulardigitalclock.persistence.LocalTimeDataSource
import fr.sully.circulardigitalclock.persistence.TimeDataSource
import fr.sully.circulardigitalclock.ui.ViewModelFactory

/**
 * Enables injection of data sources.
 */

object Injection {

    fun provideTimeDataSource(context : Context) : TimeDataSource {
        val database = AppDatabase.getInstance(context)
        return LocalTimeDataSource(database!!.timeDao())
    }

    fun provideViewModelFactory(context : Context) : ViewModelFactory {
        val dataSource = provideTimeDataSource(context)
        return ViewModelFactory(dataSource)
    }
}