package fr.sully.circulardigitalclock.persistence

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * Immutable model class for a Time
 */

@Entity(tableName = "times")
data class Time(
    @NonNull
    @PrimaryKey(autoGenerate = true)
    val id : Int = 0,

    @ColumnInfo(name = "time")
    var time : Date,

    @ColumnInfo(name = "timezoneId")
    var timezoneId : String? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        Date(parcel.readLong()),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeLong(time.time)
        parcel.writeString(timezoneId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Time> {
        override fun createFromParcel(parcel: Parcel): Time {
            return Time(parcel)
        }

        override fun newArray(size: Int): Array<Time?> {
            return arrayOfNulls(size)
        }
    }

}