package fr.sully.circulardigitalclock.persistence

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * Data Access Object for the times table.
 */

@Dao
interface TimeDao {

    /**
     * Get the time from the table.
     *
     * @return the time list from the table
     */
    @Query("SELECT * FROM times ORDER BY time")
    fun getTimes() : Flowable<List<Time>>

    /**
     * Insert a time in the database. If the time already exists, replace it.
     *
     * @param time the time to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTime(time: Time) : Completable

    /**
     * Delete a time in the database.
     *
     * @param time the time to be deleted.
     */
    @Delete
    fun deleteTime(time : Time) : Completable

    /**
     * Delete all times.
     */
    @Query("DELETE FROM times")
    fun deleteAllTimes() : Completable
}