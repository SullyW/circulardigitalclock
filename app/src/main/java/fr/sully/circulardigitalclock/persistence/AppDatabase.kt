package fr.sully.circulardigitalclock.persistence

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import fr.sully.circulardigitalclock.helper.Converters

/**
 * The Room database that contains the Times table
 */

@Database(entities = [Time::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun timeDao() : TimeDao

    companion object {
        private const val DB = "times_db"

        @Volatile
        private var INSTANCE : AppDatabase? = null

        fun getInstance(context : Context): AppDatabase? =
            INSTANCE ?: synchronized(this) {
                    INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context : Context) =
            Room
                .databaseBuilder(context.applicationContext, AppDatabase::class.java, DB)
                .fallbackToDestructiveMigration()
                .build()
    }

}