package fr.sully.circulardigitalclock.persistence

import io.reactivex.Completable
import io.reactivex.Flowable

interface TimeDataSource {

    /**
     * Gets the time from the data source.
     *
     * @return the time from the data source.
     */
    fun getTimes() : Flowable<List<Time>>

    /**
     * Inserts the time into the data source, or, if this is an existing time, updates it.
     *
     * @param time the time to be inserted or updated.
     */
    fun  insertOrUpdateTime(time: Time) : Completable

    /**
     * Deletes the time from the data source.
     */
    fun deleteTime(time : Time) : Completable

    /**
     * Deletes all times from the data source.
     */
    fun deleteAllTimes() : Completable
}