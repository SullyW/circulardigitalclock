package fr.sully.circulardigitalclock.persistence

import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * Using the Room database as a data source.
 */

class LocalTimeDataSource(private val timeDao: TimeDao) : TimeDataSource {

    override fun getTimes(): Flowable<List<Time>> = timeDao.getTimes()

    override fun insertOrUpdateTime(time: Time): Completable = timeDao.insertTime(time)

    override fun deleteTime(time: Time) : Completable = timeDao.deleteTime(time)

    override fun deleteAllTimes() : Completable = timeDao.deleteAllTimes()

}