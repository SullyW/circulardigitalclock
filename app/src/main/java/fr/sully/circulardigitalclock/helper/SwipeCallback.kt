package fr.sully.circulardigitalclock.helper

import android.content.Context
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import fr.sully.circulardigitalclock.R
import fr.sully.circulardigitalclock.ui.time.TimeAdapter

class SwipeCallback(private val mContext: Context?,
                             private val mAdapter : TimeAdapter,
                             mDragDirs : Int,
                             mSwipeDirs : Int) : ItemTouchHelper.SimpleCallback(mDragDirs, mSwipeDirs) {

    private val mClearPaint = Paint().apply { xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR) }

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        return false // We don't want drag & drop
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        mAdapter.deleteTime(viewHolder.adapterPosition)
    }

    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        val itemView = viewHolder.itemView
        val isCanceled = dX == 0f && !isCurrentlyActive

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

            if (isCanceled) {
                clearCanvas(c, itemView.right + dX, itemView.top.toFloat(), itemView.right.toFloat(), itemView.bottom.toFloat())
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                return
            }

            // Let's draw the delete view
            val deleteIcon = ContextCompat.getDrawable(mContext!!, R.drawable.ic_delete_white)
            val intrinsicWidth = deleteIcon!!.intrinsicWidth
            val intrinsicHeight = deleteIcon.intrinsicHeight

            val background = ColorDrawable()
            val itemHeight = itemView.bottom - itemView.top

            // Draw the red background
            background.apply {
                color = Color.RED
                setBounds(itemView.right + dX.toInt(), itemView.top, itemView.right, itemView.bottom)
                draw(c)
            }

            // Calculate position of delete icon
            val deleteIconTop = itemView.top + (itemHeight - intrinsicHeight) / 2
            val deleteIconMargin = (itemHeight - intrinsicHeight) / 2
            val deleteIconLeft = itemView.right - deleteIconMargin - intrinsicWidth
            val deleteIconRight = itemView.right - deleteIconMargin
            val deleteIconBottom = deleteIconTop + intrinsicHeight

            // Draw the delete icon
            deleteIcon.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom)
            deleteIcon.draw(c)
        }

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

    private fun clearCanvas(c : Canvas?, left : Float, top : Float, right : Float, bottom : Float) {
        c?.drawRect(left, top, right, bottom, mClearPaint)
    }

}