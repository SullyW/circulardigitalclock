package fr.sully.circulardigitalclock.ui.clock

import android.util.Log
import android.view.View
import androidx.core.view.ViewCompat
import androidx.lifecycle.ViewModelProviders
import fr.sully.circulardigitalclock.Injection
import fr.sully.circulardigitalclock.R
import fr.sully.circulardigitalclock.persistence.Time
import fr.sully.circulardigitalclock.ui.BaseFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*

import kotlinx.android.synthetic.main.fragment_clock.*

class ClockFragment : BaseFragment() {

    private val TAG = ClockFragment::class.java.simpleName

    private lateinit var mClockViewModel : ClockViewModel

    private var mDayOfWeekFormat : SimpleDateFormat? = null
    private var mDateFormat : SimpleDateFormat? = null
    private var mTimeFormat : SimpleDateFormat? = null
    private var mSimpleTimeFormat : SimpleDateFormat? = null
    private var mSecondsFormat : SimpleDateFormat? = null

    companion object {
        fun newInstance() : ClockFragment {
            return ClockFragment()
        }
    }

    // --------------
    // BASE METHODS
    // --------------

    override val mFragmentLayout: Int
        get() = R.layout.fragment_clock

    override fun setupViews(view: View) {
        mDayOfWeekFormat = SimpleDateFormat("EEEE", Locale.getDefault())
        mDateFormat = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
        mTimeFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        mSimpleTimeFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
        mSecondsFormat = SimpleDateFormat("ss", Locale.getDefault())

        // Get the ClockViewModel
        val viewModelFactory = Injection.provideViewModelFactory(context!!)
        mClockViewModel = ViewModelProviders.of(this, viewModelFactory).get(ClockViewModel::class.java)

        // Create the observer which updates the UI
        mClockViewModel.mCurrentTime.observe(this, androidx.lifecycle.Observer {
            updateCurrentTime(it)
        })

        // Add time
        fab_display.setOnClickListener { addTime(Time(time = mClockViewModel.getCurrentTime(), timezoneId = TimeZone.getDefault().id))}
    }

    override fun onActivityCreated() {
        updateUI()
    }

    override fun release() { }

    // --------------
    // DATA
    // --------------

    private fun addTime(time : Time) {
        mDisposable.add(mTimeViewModel.updateTime(time)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    mCallback!!.displayTime(Time(time = mClockViewModel.getCurrentTime(), timezoneId = TimeZone.getDefault().id))
                },
                { error ->
                    Log.e(TAG, "Error : $error")
                }
            )
        )
    }

    // --------------
    // UI
    // --------------

    private fun updateCurrentTime(date : Date) {
        tv_timezone.text = getString(R.string.timezone, TimeZone.getDefault().id)
        tv_day_of_week.text = mDayOfWeekFormat!!.format(date)
        tv_date.text = mDateFormat!!.format(date)
        tv_simple_time.text = mSimpleTimeFormat!!.format(date)
        tv_time.text = mTimeFormat!!.format(date)
        pb_seconds.progress = if (Integer.parseInt(mSecondsFormat!!.format(date)) == 0) pb_seconds.max else Integer.parseInt(mSecondsFormat!!.format(date))

    }

    private fun updateUI() {
        val elevation = 8f

        fab.compatElevation = elevation
        fab_display.compatElevation = elevation
        ViewCompat.setElevation(tv_timezone, elevation)
        ViewCompat.setElevation(tv_simple_time, elevation)
    }
}