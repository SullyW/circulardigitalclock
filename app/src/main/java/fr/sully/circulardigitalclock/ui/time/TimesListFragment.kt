package fr.sully.circulardigitalclock.ui.time

import android.util.Log
import android.view.View
import android.view.ViewStub
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import fr.sully.circulardigitalclock.R
import fr.sully.circulardigitalclock.helper.SwipeCallback
import fr.sully.circulardigitalclock.persistence.Time
import fr.sully.circulardigitalclock.ui.BaseFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

import kotlinx.android.synthetic.main.fragment_times_list.rv_times

class TimesListFragment : BaseFragment(), TimeAdapter.ItemViewListener {

    private val TAG = TimesListFragment::class.java.simpleName

    private lateinit var mAdapter : TimeAdapter
    private lateinit var mViewStub: ViewStub

    companion object {
        fun newInstance() : TimesListFragment {
            return TimesListFragment()
        }
    }

    // --------------
    // BASE METHODS
    // --------------

    override val mFragmentLayout: Int
        get() = R.layout.fragment_times_list

    override fun setupViews(view: View) {
        mViewStub = view.findViewById(R.id.view_stub)

        mAdapter = TimeAdapter(mListener = this)

        rv_times.apply {

            // Use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // Use a linear layout manager
            layoutManager = LinearLayoutManager(activity)

            // Add a divider between your rows
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

            // Specify an adapter
            adapter = mAdapter
        }

        val touchHandler = ItemTouchHelper(SwipeCallback(activity, mAdapter, 0, (ItemTouchHelper.LEFT)))
        touchHandler.attachToRecyclerView(rv_times)
    }

    override fun onActivityCreated() {
        subscribe()
    }

    override fun release() { }

    // --------------------
    // CALLBACK
    // --------------------

    override fun deleteTime(time: Time) {
        deleteTimeFromDb(time)
    }

    override fun displayTime(time: Time) {
        Toast.makeText(this.context, time.toString(), Toast.LENGTH_SHORT).show()
    }

    // --------------
    // DATA
    // --------------

    private fun deleteTimeFromDb(time : Time) {
        mDisposable.add(mTimeViewModel.deleteTime(time)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    Toast.makeText(this.context, R.string.delete_success, Toast.LENGTH_SHORT).show()
                },
                { error ->
                    Log.e(TAG, "Error : $error")
                }
            )
        )
    }

    private fun subscribe() {
        mDisposable.add(mTimeViewModel.getTimes()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    updateUI(it)
                },
                { error ->
                    Log.e(TAG, "Error : $error")
                }
            )
        )
    }

    // --------------
    // UI
    // --------------

    private fun updateUI(times : List<Time>) {
        mAdapter.updateTimes(times)

        if (times.isEmpty()) {
            showViewStub()
        }
        else {
           hideViewStub()
        }
    }

    private fun showViewStub() {
        if (mViewStub.parent != null) {
            mViewStub.inflate()
        }
        else {
            mViewStub.visibility = View.VISIBLE
        }
    }

    private fun hideViewStub() {
        mViewStub?.let {
            it.visibility = View.GONE
        }
    }
}
