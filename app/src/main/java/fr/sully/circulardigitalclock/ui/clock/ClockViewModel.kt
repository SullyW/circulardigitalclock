package fr.sully.circulardigitalclock.ui.clock

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.*
import java.util.concurrent.TimeUnit

class ClockViewModel : ViewModel() {

    val mCurrentTime : MutableLiveData<Date> = MutableLiveData()

    private lateinit var mDisposable : Disposable

    init {
        startTime()
    }

    override fun onCleared() {
        mDisposable.dispose()
        super.onCleared()
    }

    private fun startTime() {
        mDisposable = Observable.interval(0, 1000L, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                // Post to main thread
                mCurrentTime.postValue(Calendar.getInstance().time)
            }
            .subscribe()
    }

    fun getCurrentTime() : Date = Calendar.getInstance().time
}