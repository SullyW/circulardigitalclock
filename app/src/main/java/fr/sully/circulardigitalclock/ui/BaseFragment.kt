package fr.sully.circulardigitalclock.ui

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import fr.sully.circulardigitalclock.Injection
import fr.sully.circulardigitalclock.inflate
import fr.sully.circulardigitalclock.persistence.Time
import fr.sully.circulardigitalclock.ui.time.TimeViewModel
import io.reactivex.disposables.CompositeDisposable
import java.lang.ClassCastException

abstract class BaseFragment : Fragment() {

    protected var mCallback : FragmentListener? = null

    protected lateinit var mTimeViewModel : TimeViewModel
    protected val mDisposable = CompositeDisposable()

    protected abstract val mFragmentLayout : Int

    protected abstract fun setupViews(view : View)
    protected abstract fun onActivityCreated()
    protected abstract fun release()

    interface FragmentListener {
        fun displayTime(time: Time)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Call the extension function
        val view = container!!.inflate(mFragmentLayout, false)

        val viewModelFactory = Injection.provideViewModelFactory(context!!)
        mTimeViewModel = ViewModelProviders.of(this, viewModelFactory).get(TimeViewModel::class.java)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.setupViews(view)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        this.onActivityCreated()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        // Call the method that creating callback after being attached to parent activity
        createCallback()
    }

    override fun onDestroy() {
        mCallback = null
        release()
        super.onDestroy()
    }

    // --------------------
    // CALLBACK
    // --------------------

    private fun createCallback() {
        try {
            mCallback = activity as FragmentListener?
        }
        catch (e : ClassCastException) {
            throw ClassCastException(e.toString() + "must implement FragmentListener")
        }
    }
}