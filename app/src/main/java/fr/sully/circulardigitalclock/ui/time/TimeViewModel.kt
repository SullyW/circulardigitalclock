package fr.sully.circulardigitalclock.ui.time

import androidx.lifecycle.ViewModel
import fr.sully.circulardigitalclock.persistence.Time
import fr.sully.circulardigitalclock.persistence.TimeDataSource
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * View Model for the {@link MainActivity}
 */
class TimeViewModel(private val mDataSource : TimeDataSource) : ViewModel() {

    private var mTimes : List<Time> = ArrayList<Time>()

    /**
     * Get the time list.
     *
     * @return a [Flowable] that will emit every time the time list has been updated.
     */
    fun getTimes() : Flowable<List<Time>> {
        return mDataSource.getTimes()
            .map { time ->
                mTimes = time
                return@map mTimes
            }
    }

    /**
     * Update the time.
     *
     * @param time the new time
     * @return a [Completable] that completes when the time is updated
     */
    fun updateTime(time : Time) : Completable {
        return mDataSource.insertOrUpdateTime(time)
    }

    /**
     * Delete the time.
     *
     * @param time the time to be deleted
     * @return a [Completable] that completes when the time is deleted
     */
    fun deleteTime(time : Time) : Completable {
        return mDataSource.deleteTime(time)
    }

    /**
     * Delete the time.
     *
     * @param time the time to be deleted
     * @return a [Completable] that completes when the time is deleted
     */
    fun deleteAllTimes() : Completable {
        return mDataSource.deleteAllTimes()
    }
}