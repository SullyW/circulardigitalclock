package fr.sully.circulardigitalclock.ui.time

import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import fr.sully.circulardigitalclock.R
import fr.sully.circulardigitalclock.inflate
import fr.sully.circulardigitalclock.persistence.Time
import kotlinx.android.synthetic.main.recyclerview_time_row.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class TimeAdapter(private var mTimes : List<Time> = ArrayList(),
                  private val mListener : ItemViewListener,
                  private val mDateFormat: SimpleDateFormat = SimpleDateFormat("EEE dd MMM yyyy", Locale.getDefault()),
                  private val mTimeFormat: SimpleDateFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())) : RecyclerView.Adapter<TimeAdapter.TimeViewHolder>() {

    interface ItemViewListener {
        fun displayTime(time: Time)
        fun deleteTime(time : Time)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimeViewHolder {
        val inflateView =  parent.inflate(R.layout.recyclerview_time_row, false)
        return TimeViewHolder(inflateView)
    }

    override fun getItemCount() = mTimes.size

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: TimeViewHolder, position: Int) {
        val time = mTimes[position]
        holder.bindTime(time, mDateFormat, mTimeFormat)
        holder.itemView.setOnClickListener {mListener.displayTime(time)}
        setAnimation(holder.itemView)
    }

    fun updateTimes(times : List<Time>) {
        DiffUtil.calculateDiff(TimeRowDiffCallback(times, mTimes), false).dispatchUpdatesTo(this)
        mTimes = times
    }

    fun deleteTime(position : Int) {
        mListener.deleteTime(mTimes[position])

        val times = mTimes as ArrayList<Time>
        times.removeAt(position)
        notifyItemRemoved(position)
    }

    private fun setAnimation(v : View) {
        if (v.animation == null) {
            v.animation = AnimationUtils.loadAnimation(v.context, android.R.anim.slide_in_left)
        }
    }

    // Provide a reference to the views for each data item
    class TimeViewHolder(v : View) : RecyclerView.ViewHolder(v) {

        private var mView : View = v
        private var mTime : Time? = null

        fun bindTime(time : Time, dateFormat : SimpleDateFormat, timeFormat : SimpleDateFormat) {
            mTime = time
            mView.tv_date.text = dateFormat.format(time.time)
            mView.tv_time.text = timeFormat.format(time.time)
            mView.tv_timezone.text = time.timezoneId
        }
    }

    // This class checks the items to see if they are the same id or have the same value
    class TimeRowDiffCallback(private val mNewRows : List<Time>, private val mOldRows : List<Time>) : DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldRow = mOldRows[oldItemPosition]
            val newRow = mNewRows[newItemPosition]
            return oldRow.id == newRow.id
        }

        override fun getOldListSize() = mOldRows.size

        override fun getNewListSize() = mNewRows.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldRow = mOldRows[oldItemPosition]
            val newRow = mNewRows[newItemPosition]
            return oldRow == newRow
        }

    }
}