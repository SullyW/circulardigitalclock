package fr.sully.circulardigitalclock.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import fr.sully.circulardigitalclock.R
import fr.sully.circulardigitalclock.persistence.Time
import fr.sully.circulardigitalclock.ui.anim.ZoomOutPageTransformer
import fr.sully.circulardigitalclock.ui.clock.ClockFragment
import fr.sully.circulardigitalclock.ui.time.TimesListFragment
import java.text.SimpleDateFormat
import java.util.*

import kotlinx.android.synthetic.main.activity_main.container

/**
 * The number of pages to show.
 */

private const val NUM_PAGES = 2

/**
 * Main screen of the app.
 */

class MainActivity : AppCompatActivity(), BaseFragment.FragmentListener {

    private val TAG = MainActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setup()
    }

    override fun onBackPressed() {
        // If the user is currently looking at the first step, don't allow the system to handle the
        // Back button.
        if (container.currentItem != 0) {
            container.currentItem = container.currentItem - 1
        }
    }

    // --------------------
    // CALLBACK
    // --------------------

    override fun displayTime(time: Time) {
        display(time)
    }

    // --------------------
    // UI
    // --------------------

    private fun setup() {
        container.adapter = ScreenSlidePagerAdapter(supportFragmentManager)
        container.setPageTransformer(true, ZoomOutPageTransformer())
    }

    private fun display(time : Time) {
        val dateFormat = SimpleDateFormat("EEE dd MMM yyyy HH:mm:ss", Locale.getDefault())

        Toast.makeText(this, getString(R.string.save_success, dateFormat.format(time.time)), Toast.LENGTH_SHORT).show()
    }

    /**
     * A simple pager adapter that represents 2 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        override fun getCount(): Int = NUM_PAGES

        override fun getItem(position: Int): BaseFragment {
            return when(position) {
                0 -> ClockFragment.newInstance()
                1 -> TimesListFragment.newInstance()
                else -> {
                    Log.e(TAG, "The fragment is not implemented")
                    ClockFragment.newInstance()
                }
            }
        }
    }

}
