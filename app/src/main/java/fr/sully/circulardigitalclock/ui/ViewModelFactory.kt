package fr.sully.circulardigitalclock.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import fr.sully.circulardigitalclock.persistence.TimeDataSource
import fr.sully.circulardigitalclock.ui.clock.ClockViewModel
import fr.sully.circulardigitalclock.ui.time.TimeViewModel
import java.lang.IllegalArgumentException

/**
 * Factory for ViewModels
 */

class ViewModelFactory(private val mDataSource : TimeDataSource) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TimeViewModel::class.java)) {
            return TimeViewModel(mDataSource) as T
        }
        else if (modelClass.isAssignableFrom(ClockViewModel::class.java)) {
            return ClockViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}